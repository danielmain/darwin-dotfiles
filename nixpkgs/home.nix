{ config, pkgs, lib, ... }:

{
    programs.home-manager.enable = true;

    home.username = builtins.getEnv "USER";
    home.homeDirectory = builtins.getEnv "HOME";

    home.stateVersion = "22.11";

    # Direnv, load and unload environment variables depending on the current directory.
    # https://direnv.net
    # https://rycee.gitlab.io/home-manager/options.html#opt-programs.direnv.enable
    programs.direnv.enable = true;
    programs.direnv.nix-direnv.enable = true;

    # Htop
    # https://rycee.gitlab.io/home-manager/options.html#opt-programs.htop.enable
    #programs.htop.enable = true;
    #programs.htop.settings.show_program_path = true;

    home.packages = with pkgs; [
        #android-tools
        aria
        bat
        cabal-install
        colorls
        curl
        findutils
        fish
        gradle
        #haskellPackages.ghcup
        haskellPackages.haskus-utils-variant
        haskellPackages.retry
        haskellPackages.streamly
        nodejs
        git
        htop
        jq
        jdk11
        #neovim
        niv
        nmap
        nodePackages.typescript
        nodePackages.react-native-cli
	mc
        oh-my-zsh
        python3
        #strace
        tree
        ruby.devEnv
        yq
        htop
        wget
        zsh
        cocoapods
        m-cli
        yarn
        watchman
        wimlib
    ];

    programs.bat.enable = true;
    programs.fzf.enable = true;
    programs.fzf.enableZshIntegration = true;
    programs.gh.enable = true;

  programs.fish = {
   enable = true;

   plugins = [{
       name="foreign-env";
       src = pkgs.fetchFromGitHub {
           owner = "oh-my-fish";
           repo = "plugin-foreign-env";
           rev = "dddd9213272a0ab848d474d0cbde12ad034e65bc";
           sha256 = "00xqlyl3lffc5l0viin1nyp819wf81fncqyz87jx8ljjdhilmgbs";
       };
   }];

   shellInit =
     ''
       export ANDROID_HOME=$HOME/Library/Android/sdk
   export NIX_PATH="darwin-config=$HOME/.nixpkgs/darwin-configuration.nix:$HOME/.nix-defexpr/channels"
   alias vim="nvim"
   export PATH="/nix/var/nix/profiles/default/bin:$HOME/.nix-profile/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator:$ANDROID_HOME/tools:$PATH"
   '';
  };

  programs.neovim = {
    enable = true;
    viAlias = true;
    plugins = with pkgs.vimPlugins; [
      coc-css
      coc-lua
      coc-yaml
      coc-json
      coc-jest
      coc-html
      coc-eslint
      coc-tabnine
      coc-tsserver
      coc-prettier
      coc-explorer
      coc-fzf
      coc-nvim
      coc-git
      coc-json
      coc-vimlsp
      ctrlp-z
      deoplete-nvim
      diffview-nvim
      fzf-vim
      fzf-lua
      ghcid
      git-blame-nvim
      gruvbox-nvim
      haskell-vim
      impatient-nvim
      lazygit-nvim
      nerdtree
      nerdtree-git-plugin
      neovim-fuzzy
      nvim-cmp
      nvim-web-devicons
      rainbow
      rust-vim
      telescope-nvim
      vimagit
      vim-airline
      vim-devicons
      vim-hindent
      vim-fugitive
      vim-nerdtree-tabs
      #vim-nerdtree-syntax-highlight
      vim-nix
      vim-lua
      vim-wayland-clipboard
      vimsence
      YouCompleteMe
    ];
    extraConfig = ''
      set mouse=a
      set nu
      filetype plugin indent on

      nnoremap <leader>ff <cmd>Telescope find_files<cr>
      nnoremap <leader>fg <cmd>Telescope live_grep<cr>
      nnoremap <leader>fb <cmd>Telescope buffers<cr>
      nnoremap <leader>fh <cmd>Telescope help_tags<cr>

      " Using Lua functions
      nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
      nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
      nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
      nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

      " Start NERDTree when Vim starts with a directory argument.
      autocmd StdinReadPre * let s:std_in=1
      autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
      \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif

      " Exit Vim if NERDTree is the only window remaining in the only tab.
      autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

      let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
      let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
      let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
      let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
      let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
      let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
      let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

      let g:haskell_indent_if = 3
      let g:haskell_indent_case = 2
      let g:haskell_indent_let = 4
      let g:haskell_indent_where = 6
      let g:haskell_indent_before_where = 2
      let g:haskell_indent_after_bare_where = 2
      let g:haskell_indent_do = 3
      let g:haskell_indent_in = 1
      let g:haskell_indent_guard = 2
      let g:cabal_indent_section = 2
    '';
  };


    programs.git = {
        enable = true;
        userName = "Daniel Main";
        userEmail = "daniel.main.cernhoff@icloud.com";
        signing.signByDefault = true;
        signing.key = "0D0B5E7D";
        ignores = [ "*~" ".DS_Store" ];
        aliases = {
            unstage = "reset HEAD --";
            pr = "pull --rebase";
            addp = "add --patch";
            comp = "commit --patch";
            co = "checkout";
            ci = "commit";
            c = "commit";
            b = "branch";
            p = "push";
            d = "diff";
            a = "add";
            s = "status";
            f = "fetch";
            br = "branch";
            pa = "add --patch";
            pc = "commit --patch";
            rf = "reflog";
            l = "log --graph --pretty='%Cred%h%Creset - %C(bold blue)<%an>%Creset %s%C(yellow)%d%Creset %Cgreen(%cr)' --abbrev-commit --date=relative";
            pp = "!git push --set-upstream origin $(git rev-parse --abbrev-ref HEAD)";
            recent-branches = "branch --sort=-committerdate";
        };
        extraConfig = {
            core = {
                editor = "nvim";
            };
            url = {
                "git@github.com:" = {
                    insteadOf = "https://github.com/";
                };
            };
            pull = {
                rebase = false;
            };
            # Sign all commits using ssh key
            commit.gpgsign = true;
        };
    };

    # https://docs.haskellstack.org/en/stable/yaml_configuration/#non-project-specific-config
    home.file.".stack/config.yaml".text = lib.generators.toYAML {} {
        templates = {
            scm-init = "git";
            params = {
                author-name = "Daniel Main"; # config.programs.git.userName;
                author-email = "daniel.main.cernhoff@icloud.com"; # config.programs.git.userEmail;
                github-username = "danielmain";
            };
        };
        nix.enable = true;
    };

}
